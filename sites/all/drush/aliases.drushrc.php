<?php
/**
 * @file
 * Define drush aliases for the current project.
 */

/**
 * Alias for local environment.
 *
 * Assumes <site-name>_loc/<site-name>_loc as username/password for the database
 * and admin/admin for the website administrator username/password. YOU SHOULD CHANGE THIS.
 */

$loc_db_url = 'mysql://euci_loc:euci_loc@127.0.0.1/euci_loc';

$aliases['euci.loc'] = array(
  'uri' => 'loc.euci.com',
  'root' => '/vagrant/www/loc',
  'db-url' => $loc_db_url,
  'path-aliases' => array(
    '%drush' => '/vagrant/www/loc/vendor/drush/drush',
    '%drush-script' => '/vagrant/www/loc/bin/drush',
    '%dump-dir' => '/vagrant/www/loc/dumps',
    '%files' => 'sites/default/files',
  ),
  'target-command-specific' => array(
    'sql-sync' => array(
      'no-cache' => TRUE,
    ),
  ),
  'command-specific' => array(
    'site-install' => array(
      'site-name' => 'euci',
      'site-mail' => 'info@loc.euci.com',
      'db-url'    => $loc_db_url,
      'account-mail' => 'admin@loc.euci.com',
      'account-name' => 'admin',
      'account-pass' => 'admin',
    ),
  ),
);

/**
 * Alias for development environment.
 *
 * You should at least ensure 'root' path is correct.
 */
// $aliases['euci.dev'] = array(
//   'uri' => 'COMPLETE_ME',
//   'root' => 'COMPLETE_ME',
//   'remote-host' => 'localhost',
//   'remote-user' => 'vagrant',
//   'command-specific' => array(
//     'sql-sync' => array(
//       'no-cache' => TRUE,
//     ),
//   ),
// );

/**
 * Alias for staging environment.
 *
 * You should at least ensure 'root' path is correct.
 */
// $aliases['euci.stage'] = array(
//   'uri' => 'stage.euci.com',
//   'root' => '/var/www/euci.com/stage',
//   'remote-host' => 'stage.euci.com',
//   'remote-user' => 'root',
//   'command-specific' => array(
//     'sql-sync' => array(
//       'no-cache' => TRUE,
//     ),
//   ),
// );
