<?php
/**
 * @file
 * euci_global.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function euci_global_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
