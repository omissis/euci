<?php

namespace Drupal\Ignite;

use Behat\Mink\Exception\ElementNotFoundException;
use SensioLabs\Behat\PageObjectExtension\PageObject\Exception\UnexpectedPageException;
use SensioLabs\Behat\PageObjectExtension\PageObject\Page as BasePage;

abstract class Page extends BasePage
{
    public function openWithoutVerification(array $urlParameters = array())
    {
        $url = $this->getUrl($urlParameters);

        $this->getSession()->visit($url);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function find($selector, $locator)
    {
        $items = parent::find($selector, $locator);

        if (empty($items)) {
            throw new ElementNotFoundException($this->getSession(), null, $selector, $locator);
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll($selector, $locator)
    {
        $items = parent::findAll($selector, $locator);

        if (empty($items)) {
            throw new ElementNotFoundException($this->getSession(), null, $selector, $locator);
        }

        return $items;
    }

    /**
     * @param array $urlParameters
     *
     * @return string
     */
    protected function getUrl(array $urlParameters = array())
    {
        $path = $this->getPath() . '?' . http_build_query($urlParameters);

        $baseUrl = rtrim($this->getParameter('base_url'), '/').'/';

        return 0 !== strpos($path, 'http') ? $baseUrl . ltrim($path, '/') : $path;
    }
}
