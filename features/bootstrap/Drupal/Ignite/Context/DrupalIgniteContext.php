<?php

namespace Drupal\Ignite\Context;

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use SensioLabs\Behat\PageObjectExtension\Context\PageObjectAware;
use SensioLabs\Behat\PageObjectExtension\Context\PageObjectContext;
use SensioLabs\Behat\PageObjectExtension\PageObject\Factory as PageObjectFactory;

class DrupalIgniteContext extends RawDrupalContext implements SnippetAcceptingContext, PageObjectAware
{
    /**
     * @Given I try to open :pageName page
     * @Given I try to open the :pageName page
     */
    public function iTryToOpenPage($pageName)
    {
        $this->getPage($pageName)->openWithoutVerification();
    }

    /**
     * @Given I open :pageName page
     * @Given I open the :pageName page
     * @Given I am on :pageName page
     * @Given I am on the :pageName page
     */
    public function iOpenPage($pageName)
    {
        $this->getPage($pageName)->open();
    }

    /**
     * @Given I open :pageName page with the following parameters:
     * @Given I open the :pageName page with the following parameters:
     * @Given I am on :pageName page with the following parameters:
     * @Given I am on the :pageName page with the following parameters:
     */
    public function iOpenPageWithTheFollowingParameters($pageName, TableNode $table)
    {
        $pageName = str_replace(' ', '', ucwords($pageName));
        $dates = $table->getRowsHash();

        $this->getPage($pageName)->open($dates);
    }

    /**
     * @Given /^I am logged in as a user without the "([^"]*)" permission$/
     */
    public function iAmLoggedInAsAUserWithoutThePermission($permission) {
        // Create user.
        $user = (object) array(
          'name' => $this->getRandom()->name(8),
          'pass' => $this->getRandom()->name(16),
        );
        $user->mail = "{$user->name}@example.com";
        $this->userCreate($user);

        // Login.
        $this->login();
    }

    /**
     * @param PageObjectFactory $pageObjectFactory
     */
    public function setPageObjectFactory(PageObjectFactory $pageObjectFactory)
    {
        if (empty($this->pageObjectContext) || $this->pageObjectContext instanceof PageObjectContext) {
            $this->pageObjectContext = new PageObjectContext();
        }

        $this->pageObjectContext->setPageObjectFactory($pageObjectFactory);
    }

    public function getPage($pageName)
    {
        return $this->pageObjectContext->getPage($pageName . 'Page');
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->pageObjectContext, $name)) {
            return call_user_func_array(array($this->pageObjectContext, $name), $arguments);
        }

        throw new \RuntimeException("Method '$name' does not exist.");
    }
}
